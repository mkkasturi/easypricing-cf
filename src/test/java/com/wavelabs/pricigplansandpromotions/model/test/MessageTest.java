package com.wavelabs.pricigplansandpromotions.model.test;

import org.junit.Assert;
import org.junit.Test;

import com.wavelabs.pricingplansandpromotions.model.Message;


public class MessageTest {
	@Test
	public void testGetId() {
		Message message = new Message();
		message.setId(12);
		Assert.assertEquals(12, message.getId());
	}

	@Test
	public void testGetMessage() {
		Message message = new Message();
		message.setMessage("File Not Found");
		Assert.assertEquals("File Not Found", message.getMessage());
	}
}
