package com.wavelabs.pricingplansandpromotions.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wavelabs.pricingplansandpromotions.model.User;
import com.wavelabs.pricingplansandpromotions.repository.UserRepository;

@Component
public class UserService {

	@Autowired
	UserRepository userRepo;

	@PersistenceContext
	EntityManager entityManager;

	public User getUser(int userId) {
		return userRepo.findOne(userId);
	}

	public List<User> getAllUsers() {
		return (List<User>) userRepo.findAll();
	}

	@Transactional
	public User updateUser(User user) {
		return entityManager.merge(user);
	}

	public User deleteUser(int userId) {
		User user = userRepo.findOne(userId);
		if (user != null) {
			userRepo.delete(userId);
			return user;
		} else {
			return null;
		}
	}
}
