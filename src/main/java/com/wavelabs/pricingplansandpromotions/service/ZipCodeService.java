package com.wavelabs.pricingplansandpromotions.service;

import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wavelabs.pricingplansandpromotions.model.Scheme;
import com.wavelabs.pricingplansandpromotions.model.ZipCode;
import com.wavelabs.pricingplansandpromotions.repository.ZipCodeRepository;

@Component
public class ZipCodeService {

	@Autowired
	ZipCodeRepository zipCodeRepo;

	@Autowired
	SchemeService schemeService;

	@PersistenceContext
	EntityManager entityManager;

	private final static Logger LOGGER = Logger.getLogger(PlanService.class.getName());

	public boolean addZipCode(ZipCode zipCode) {
		try {
			zipCodeRepo.save(zipCode);
			return true;
		} catch (Exception e) {
			LOGGER.error(e);
			return false;
		}
	}

	public ZipCode getZipCode(int zipCodeId) {
		return zipCodeRepo.findOne(zipCodeId);
	}

	@Transactional
	public ZipCode addSchemeToZip(int schemeId, int zipCodeId) {
		ZipCode zipCode = getZipCode(zipCodeId);
		Scheme scheme = schemeService.getScheme(schemeId);
		Set<Scheme> schemes = zipCode.getSchemes();
		schemes.add(scheme);
		return entityManager.merge(zipCode);
	}
}
