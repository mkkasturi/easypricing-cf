package com.wavelabs.pricingplansandpromotions.service;

import java.util.Calendar;
import java.util.Date;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wavelabs.pricingplansandpromotions.model.DiscountType;
import com.wavelabs.pricingplansandpromotions.model.Plan;
import com.wavelabs.pricingplansandpromotions.model.PromoCode;
import com.wavelabs.pricingplansandpromotions.model.User;
import com.wavelabs.pricingplansandpromotions.repository.PromoCodeRepository;

@Component
public class PromoCodeService {

	@Autowired
	PromoCodeRepository promoCodeRepo;

	@Autowired
	PlanService planService;

	@Autowired
	UserService userService;

	private final static Logger LOGGER = Logger.getLogger(PromoCodeService.class.getName());

	public boolean addPromoCode(PromoCode promoCode) {
		try {
			promoCodeRepo.save(promoCode);
			return true;
		} catch (Exception e) {
			LOGGER.error(e);
			return false;
		}

	}

	public PromoCode getPromoCode(int promoCodeId) {
		return promoCodeRepo.findOne(promoCodeId);
	}

	public Object[] getAmount(int planId, String promoCode, int userId) {
		double amount = 0;
		int error;
		PromoCode promo;
		promo = promoCodeRepo.findByCode(promoCode);
		if (promo != null) {
			Date expiryDate = promo.getExpiresOn();
			Date date = Calendar.getInstance().getTime();
			DiscountType discountType = promo.getDiscountType();
			Set<Integer> months = promo.getNoOfMonthsApplicable();
			double discount = promo.getDiscountAmount();
			if (expiryDate.after(date)) {
				User user = userService.getUser(userId);
				Set<PromoCode> promoCodes = user.getUsedPromoCodes();
				if (!(promoCodes.contains(promo))) {
					Plan plan;
					if (planService.isPlan(planId)) {
						plan = planService.getPlan(planId);
					} else {
						plan = planService.getPlanAfterFreeTrial(planId);
					}
					double price = plan.getPrice();
					Integer planMonths = plan.getDurationInMonths();
					if (months.contains(planMonths)) {
						if (discountType.equals(DiscountType.PERCENTAGE)) {
							amount = price - ((discount / 100) * price);
						} else {
							amount = price - discount;
						}
						LOGGER.info("The amount to pay: " + amount);
						error = 0;
						return new Object[] { amount, error };

					} else {
						LOGGER.info("The Promo code you have entered is not applicable for your Plan!");
						error = 1;
						return new Object[] { amount, error };
					}

				} else {
					LOGGER.info("Sorry you have already used this promo code!");
					error = 2;
					return new Object[] { amount, error };
				}
			} else {
				LOGGER.info("The Promo code you have entered is expired!");
				error = 3;
				return new Object[] { amount, error };
			}
		} else {
			LOGGER.info("The Promo Code you have entered doesn't exist!");
			error = 4;
			return new Object[] { amount, error };
		}

	}
}
