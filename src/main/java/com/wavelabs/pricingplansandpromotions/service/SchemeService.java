package com.wavelabs.pricingplansandpromotions.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wavelabs.pricingplansandpromotions.model.Scheme;
import com.wavelabs.pricingplansandpromotions.model.User;
import com.wavelabs.pricingplansandpromotions.model.ZipCode;
import com.wavelabs.pricingplansandpromotions.repository.SchemeRepository;
import com.wavelabs.pricingplansandpromotions.repository.UserRepository;

@Component
public class SchemeService {
	@Autowired
	SchemeRepository schemeRepo;

	@Autowired
	ZipCodeService zipCodeService;

	@Autowired
	UserRepository userRepo;
	
	@Autowired
	UserService userService;

	@PersistenceContext
	EntityManager entityManager;

	public Scheme addScheme(Scheme scheme) {
		return schemeRepo.save(scheme);
	}

	public Scheme getScheme(int schemeId) {
		return schemeRepo.findOne(schemeId);
	}

	public Scheme getRandomScheme(int zipCodeId, int userId) {
		User user = userRepo.findOne(userId);
		Scheme scheme = user.getScheme();
		if (scheme == null) {
			ZipCode zip = zipCodeService.getZipCode(zipCodeId);
			Set<Scheme> schemes = zip.getSchemes();
			List<Scheme> list = new ArrayList<>();
			list.addAll(schemes);
			Collections.shuffle(list);
			Scheme randomScheme = list.get(0);
			user.setScheme(randomScheme);
			userService.updateUser(user);
			return randomScheme;
		} else {
			return scheme;
		}
	}

	@Transactional
	public Scheme updateScheme(Scheme scheme) {
		return entityManager.merge(scheme);
	}
}
