package com.wavelabs.pricingplansandpromotions.repository;

import org.springframework.data.repository.CrudRepository;

import com.wavelabs.pricingplansandpromotions.model.Scheme;

public interface SchemeRepository extends CrudRepository<Scheme, Integer> {

}
