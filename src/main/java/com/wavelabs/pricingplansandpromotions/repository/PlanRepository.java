package com.wavelabs.pricingplansandpromotions.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.wavelabs.pricingplansandpromotions.model.Plan;

public interface PlanRepository extends CrudRepository<Plan, Integer> {

	@Query("select p from Scheme s inner join s.plans p where s.id=:schemeId ORDER BY p.price")
	List<Plan> getPlansByOrder(@Param("schemeId") int schemeId);
}
