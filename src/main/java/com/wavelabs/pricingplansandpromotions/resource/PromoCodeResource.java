package com.wavelabs.pricingplansandpromotions.resource;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.pricingplansandpromotions.model.PromoCode;
import com.wavelabs.pricingplansandpromotions.model.TokenInfo;
import com.wavelabs.pricingplansandpromotions.service.PromoCodeService;
import com.wavelabs.pricingplansandpromotions.utility.UserTypeChecker;

import io.nbos.capi.api.v0.models.NotFoundResponse;
import io.nbos.capi.api.v0.models.RestMessage;

@RestController
@Component
public class PromoCodeResource {
	
	private static String forbiddenMessage = "Please Login";

	@Autowired
	PromoCodeService promoCodeService;

	private final static Logger LOGGER = Logger.getLogger(PromoCodeResource.class.getName());

	@RequestMapping(value = "promocodes/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getPromoCode(@PathVariable("id") int id) {
		PromoCode promoCode = promoCodeService.getPromoCode(id);
		if (promoCode != null) {
			return ResponseEntity.status(200).body(promoCode);
		} else {
			NotFoundResponse restMessage = new NotFoundResponse();
			restMessage.message = "Promo code not Found";
			restMessage.messageCode = "404";
			return ResponseEntity.status(404).body(restMessage);
		}
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/promocodes", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity addPromoCode(@RequestBody PromoCode code,
			@RequestAttribute("tokenInfo") TokenInfo tokenInfo) {
		boolean flag = promoCodeService.addPromoCode(code);
		boolean guestUserFlag = UserTypeChecker.isGuestUser(tokenInfo);
		if (!guestUserFlag) {
			RestMessage restMessage = new RestMessage();
			if (flag) {
				restMessage.message = "Promo code created Successfully!";
				restMessage.messageCode = "200";
				return ResponseEntity.status(200).body(restMessage);
			} else {
				restMessage.messageCode = "500";
				restMessage.message = "Promo code creation failed!";
				return ResponseEntity.status(500).body(restMessage);
			}
		} else {
			RestMessage restMessage = new RestMessage();
			restMessage.messageCode = "403";
			restMessage.message = forbiddenMessage;
			return ResponseEntity.status(403).body(restMessage);
		}
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "promocodes/getamount", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity getAmount(@RequestParam("planId") int planId, @RequestParam("promocode") String promocode,
			@RequestParam("memberId") int memberId, @RequestAttribute("tokenInfo") TokenInfo tokenInfo) {
		boolean guestUserFlag = UserTypeChecker.isGuestUser(tokenInfo);
		if (!guestUserFlag) {
			try {
				Object[] result = promoCodeService.getAmount(planId, promocode, memberId);
				double amount = (double) result[0];
				int error = (int) result[1];
				RestMessage restMessage = new RestMessage();
				if (error == 0) {
					restMessage.message = "The amount you have to pay: Rs." + amount;
					restMessage.messageCode = "200";
					return ResponseEntity.status(200).body(restMessage);
				} else if (error == 1) {
					restMessage.message = "The Promo code you have entered is not applicable for your Plan!";
					restMessage.messageCode = "200";
					return ResponseEntity.status(200).body(restMessage);
				} else if (error == 2) {
					restMessage.message = "Sorry you have already used this promo code!";
					restMessage.messageCode = "200";
					return ResponseEntity.status(200).body(restMessage);
				} else if (error == 3) {
					restMessage.message = "The Promo code you have entered is expired!";
					restMessage.messageCode = "200";
					return ResponseEntity.status(200).body(restMessage);
				} else {
					restMessage.message = "The Promo Code you have entered doesn't exist!";
					restMessage.messageCode = "404";
					return ResponseEntity.status(404).body(restMessage);
				}

			} catch (NullPointerException ne) {
				LOGGER.log(Level.SEVERE, "Exception: ", ne);
				RestMessage restMessage = new RestMessage();
				restMessage.message = "The Promo Code you have entered doesn't exist!";
				restMessage.messageCode = "404";
				return ResponseEntity.status(404).body(restMessage);
			}
		} else {
			RestMessage restMessage = new RestMessage();
			restMessage.messageCode = "403";
			restMessage.message = forbiddenMessage;
			return ResponseEntity.status(403).body(restMessage);
		}
	}
}
