package com.wavelabs.pricingplansandpromotions.resource;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.pricingplansandpromotions.model.Plan;
import com.wavelabs.pricingplansandpromotions.model.TokenInfo;
import com.wavelabs.pricingplansandpromotions.model.User;
import com.wavelabs.pricingplansandpromotions.service.PlanService;
import com.wavelabs.pricingplansandpromotions.utility.UserTypeChecker;

import io.nbos.capi.api.v0.models.InternalErrorResponse;
import io.nbos.capi.api.v0.models.NotFoundResponse;
import io.nbos.capi.api.v0.models.RestMessage;
import io.nbos.capi.api.v0.models.SuccessResponse;

@RestController
@Component
public class PlanResource {

	private static String forbiddenMessage = "Please Login";

	@Autowired
	PlanService planService;
	private final static Logger LOGGER = Logger.getLogger(PlanResource.class.getName());

	@RequestMapping(value = "plans/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getPlan(@PathVariable("id") int id) {
		Plan plan = planService.getPlan(id);
		if (plan != null) {
			return ResponseEntity.status(200).body(plan);
		} else {
			NotFoundResponse restMessage = new NotFoundResponse();
			restMessage.message = "Plan not Found";
			restMessage.messageCode = "404";
			return ResponseEntity.status(404).body(restMessage);
		}
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/plans", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity addPlan(@RequestBody Plan plan, @RequestParam("schemeId") int schemeId,
			@RequestAttribute("tokenInfo") TokenInfo tokenInfo) {
		boolean guestUserFlag = UserTypeChecker.isGuestUser(tokenInfo);
		if (!guestUserFlag) {
			boolean flag = planService.addPlan(plan, schemeId);
			RestMessage restMessage = new RestMessage();
			if (flag) {
				restMessage.message = "Plan created Successfully!";
				restMessage.messageCode = "200";
				return ResponseEntity.status(200).body(restMessage);
			} else {
				restMessage.messageCode = "500";
				restMessage.message = "Plan creation failed!";
				return ResponseEntity.status(500).body(restMessage);
			}
		} else {
			RestMessage restMessage = new RestMessage();
			restMessage.messageCode = "403";
			restMessage.message = forbiddenMessage;
			return ResponseEntity.status(403).body(restMessage);
		}
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/plans/{id}", method = RequestMethod.DELETE)
	public ResponseEntity deletePlan(@PathVariable("id") int id, @RequestAttribute("tokenInfo") TokenInfo tokenInfo) {
		boolean guestUserFlag = UserTypeChecker.isGuestUser(tokenInfo);
		if (!guestUserFlag) {
			Plan plan = planService.deletePlan(id);
			if (plan != null) {
				SuccessResponse success = new SuccessResponse();
				success.message = "Plan deleted successfully!";
				success.messageCode = "200";
				return ResponseEntity.status(200).body(success);
			} else {
				InternalErrorResponse errorResponse = new InternalErrorResponse();
				errorResponse.message = "Plan deletion Failed!";
				errorResponse.messageCode = "500";
				return ResponseEntity.status(500).body(errorResponse);
			}
		} else {
			RestMessage restMessage = new RestMessage();
			restMessage.messageCode = "403";
			restMessage.message = forbiddenMessage;
			return ResponseEntity.status(403).body(restMessage);
		}
	}

	@RequestMapping(value = "plans/planafterfreetrial/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getPlanAfterFreeTrial(@PathVariable("id") int id) {
		Plan plan = null;
		try {
			plan = planService.getPlanAfterFreeTrial(id);
		} catch (NullPointerException e) {
			LOGGER.log(Level.SEVERE, "Exception: ", e);
			NotFoundResponse restMessage = new NotFoundResponse();
			restMessage.message = "Free Trial not Found";
			restMessage.messageCode = "404";
			return ResponseEntity.status(404).body(restMessage);
		}
		if (plan != null) {
			return ResponseEntity.status(200).body(plan);
		} else {
			NotFoundResponse restMessage = new NotFoundResponse();
			restMessage.message = "Free Trial not Found";
			restMessage.messageCode = "404";
			return ResponseEntity.status(404).body(restMessage);
		}
	}

	@RequestMapping(value = "/plans/plansubscription", method = RequestMethod.PUT, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<User> makePayment(@RequestParam("amount") double amount, @RequestParam("userId") int userId,
			@RequestParam("promoId") int promoId, @RequestParam("planId") int planId) {
		return ResponseEntity.status(200).body(planService.makePayment(amount, userId, promoId, planId));
	}

	@RequestMapping(value = "/plans/freetrialsubscription", method = RequestMethod.PUT, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<User> subscribeFreeTrial(@RequestParam("planId") int planId,
			@RequestParam("userId") int userId, @RequestParam("promoCode") String promoCode) {
		return ResponseEntity.status(200).body(planService.freeTrial(planId, userId, promoCode));
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/plans/renewal", method = RequestMethod.PUT, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity renewalPlan() {
		planService.renewalPlan();
		SuccessResponse success = new SuccessResponse();
		success.message = "Plan Renewed successfully!";
		success.messageCode = "200";
		return ResponseEntity.status(200).body(success);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/plans/cancelplan", method = RequestMethod.PUT, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity cancelPlan(@RequestParam("userId") int userId,
			@RequestAttribute("tokenInfo") TokenInfo tokenInfo) {
		boolean guestUserFlag = UserTypeChecker.isGuestUser(tokenInfo);
		if (!guestUserFlag) {
			boolean flag = planService.cancelPlan(userId);
			RestMessage restMessage = new RestMessage();
			if (flag) {
				restMessage.message = "Plan cancelled Successfully!";
				restMessage.messageCode = "200";
				return ResponseEntity.status(200).body(restMessage);
			} else {
				restMessage.messageCode = "500";
				restMessage.message = "Plan cancellation failed!";
				return ResponseEntity.status(500).body(restMessage);
			}
		} else {
			RestMessage restMessage = new RestMessage();
			restMessage.messageCode = "403";
			restMessage.message = forbiddenMessage;
			return ResponseEntity.status(403).body(restMessage);
		}
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/plans/changeplan", method = RequestMethod.PUT, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity changePlan(@RequestParam("userId") int userId, @RequestParam("planId") int planId,
			@RequestAttribute("tokenInfo") TokenInfo tokenInfo) {
		boolean guestUserFlag = UserTypeChecker.isGuestUser(tokenInfo);
		if (!guestUserFlag) {
			planService.changePlan(userId, planId);
			SuccessResponse success = new SuccessResponse();
			success.message = "Plan Changed successfully!";
			success.messageCode = "200";
			return ResponseEntity.status(200).body(success);
		} else {
			RestMessage restMessage = new RestMessage();
			restMessage.messageCode = "403";
			restMessage.message = forbiddenMessage;
			return ResponseEntity.status(403).body(restMessage);
		}
	}
}
