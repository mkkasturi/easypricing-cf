package com.wavelabs.pricingplansandpromotions.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "user")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "uuid")
	private String uuid;

	@Column(name = "tenant_id")
	private String tenantId;

	@ManyToOne
	@JoinColumn(name = "plan_id")
	private Plan plan;

	@ManyToOne
	@JoinColumn(name = "scheme_id")
	private Scheme scheme;

	@ManyToOne
	@JoinColumn(name = "next_subscription_plan_id")
	private Plan nextSubscriptionPlan;

	@Column(name = "plan_renewal_date")
	private Date planRenewalDate;

	@Column(name = "free_trial_expiry_date")
	private Date freeTrialExpiryDate;

	@ManyToOne
	@JoinColumn(name = "freetrial_promocode_id")
	private PromoCode freeTrialPromoCode;

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	@JoinTable(name = "member_promocode", joinColumns = {
			@JoinColumn(name = "used_promocode_id") }, inverseJoinColumns = { @JoinColumn(name = "member_id") })
	private Set<PromoCode> usedPromoCodes;

	@Column(name = "amount_paid_for_present_plan")
	private double amountPaidForPresentPlan;

	public User() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Plan getPlan() {
		return plan;
	}

	public void setPlan(Plan plan) {
		this.plan = plan;
	}

	public Scheme getScheme() {
		return scheme;
	}

	public void setScheme(Scheme scheme) {
		this.scheme = scheme;
	}

	public Plan getNextSubscriptionPlan() {
		return nextSubscriptionPlan;
	}

	public void setNextSubscriptionPlan(Plan nextSubscriptionPlan) {
		this.nextSubscriptionPlan = nextSubscriptionPlan;
	}

	public Date getPlanRenewalDate() {
		return planRenewalDate;
	}

	public void setPlanRenewalDate(Date planRenewalDate) {
		this.planRenewalDate = planRenewalDate;
	}

	public Date getFreeTrialExpiryDate() {
		return freeTrialExpiryDate;
	}

	public void setFreeTrialExpiryDate(Date freeTrialExpiryDate) {
		this.freeTrialExpiryDate = freeTrialExpiryDate;
	}

	public PromoCode getFreeTrialPromoCode() {
		return freeTrialPromoCode;
	}

	public void setFreeTrialPromoCode(PromoCode freeTrialPromoCode) {
		this.freeTrialPromoCode = freeTrialPromoCode;
	}

	public Set<PromoCode> getUsedPromoCodes() {
		return usedPromoCodes;
	}

	public void setUsedPromoCodes(Set<PromoCode> usedPromoCodes) {
		this.usedPromoCodes = usedPromoCodes;
	}

	public double getAmountPaidForPresentPlan() {
		return amountPaidForPresentPlan;
	}

	public void setAmountPaidForPresentPlan(double amountPaidForPresentPlan) {
		this.amountPaidForPresentPlan = amountPaidForPresentPlan;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
}
