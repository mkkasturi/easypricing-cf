package com.wavelabs.pricingplansandpromotions.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "promocode")
public class PromoCode {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "code", unique=true, nullable=false)
	private String code;

	@Column(name = "discount_Amount", nullable=false)
	private double discountAmount;

	@Column(name = "valid_from", nullable=false)
	private Date validFrom;

	@Column(name = "expires_on", nullable=false)
	private Date expiresOn;

	@Enumerated(EnumType.STRING)
	@Column(name = "discount_type", nullable=false)
	private DiscountType discountType;

	@ElementCollection
	@CollectionTable(joinColumns = @JoinColumn(name = "promocode_id", referencedColumnName = "id"))
	@Column(name = "no_of_months")
	private Set<Integer> noOfMonthsApplicable;

	public PromoCode() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public double getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(double discountAmount) {
		this.discountAmount = discountAmount;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Set<Integer> getNoOfMonthsApplicable() {
		return noOfMonthsApplicable;
	}

	public void setNoOfMonthsApplicable(Set<Integer> noOfMonthsApplicable) {
		this.noOfMonthsApplicable = noOfMonthsApplicable;
	}

	public Date getExpiresOn() {
		return expiresOn;
	}

	public void setExpiresOn(Date expiresOn) {
		this.expiresOn = expiresOn;
	}

	public DiscountType getDiscountType() {
		return discountType;
	}

	public void setDiscountType(DiscountType discountType) {
		this.discountType = discountType;
	}

}
