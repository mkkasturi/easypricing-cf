package com.wavelabs.pricingplansandpromotions.model;

public class Authorities {

	private int id;
	private String uAuthorityName;
	private String displayName;
	private String description;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getuAuthorityName() {
		return uAuthorityName;
	}

	public void setuAuthorityName(String uAuthorityName) {
		this.uAuthorityName = uAuthorityName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
