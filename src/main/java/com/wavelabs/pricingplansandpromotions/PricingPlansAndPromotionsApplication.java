package com.wavelabs.pricingplansandpromotions;

import javax.servlet.Filter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.web.DispatcherServletAutoConfiguration;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.DispatcherServlet;

import com.wavelabs.pricingplansandpromotios.filterCheckings.UserCheckings;
import com.wavelabs.pricingplansandpromotios.filterPersist.ObjectPersister;


@SpringBootApplication
@EntityScan("com.wavelabs.pricingplansandpromotions.model")
@EnableJpaRepositories("com.wavelabs.pricingplansandpromotions.repository")
@ComponentScan({
		"com.wavelabs.pricingplansandpromotions.repository, com.wavelabs.pricingplansandpromotions.model, com.wavelabs.pricingplansandpromotions, com.wavelabs.pricingplansandpromotions.service, com.wavelabs.pricingplansandpromotions.resource" })
public class PricingPlansAndPromotionsApplication {

	public static void main(String[] args) {
		SpringApplication.run(PricingPlansAndPromotionsApplication.class, args);
	}
	
	@Bean
	public Filter IDNFilter() {
		return new IDNFilter();
	}
	
	@Bean
	public UserCheckings userCheckings(){
		return new UserCheckings();
	}
	
	@Bean
	public ObjectPersister userPersister(){
		return new ObjectPersister();
	}
	
	@Bean
	public AuthenticationTokenCache authenticationTokenCache() {
		return new AuthenticationTokenCache();
	}

	@Bean
	public ServletRegistrationBean dispatcherServletRegistration() {
		ServletRegistrationBean registration = new ServletRegistrationBean(
				dispatcherServlet(), "/*");
		registration
				.setName(DispatcherServletAutoConfiguration.DEFAULT_DISPATCHER_SERVLET_REGISTRATION_BEAN_NAME);
		return registration;
	}

	/**
	 * To add your own servlet and map it to the root resource. Default would be
	 * root of your application(/)
	 * 
	 * @return DispatcherServlet
	 */
	@Bean
	public DispatcherServlet dispatcherServlet() {
		return new DispatcherServlet();
	}
}
