package com.wavelabs.pricingplansandpromotios.filterPersist;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wavelabs.pricingplansandpromotions.model.User;
import com.wavelabs.pricingplansandpromotions.repository.UserRepository;


/**
 * 
 * @author muralikrishnak
 */
@Component
public class ObjectPersister {

	Logger logger = Logger.getLogger(ObjectPersister.class);
	@Autowired
	UserRepository userRepo;

	public boolean persistUser(User user) {
		boolean status = false;
		try {
			userRepo.save(user);
			status = true;
		} catch (Exception e) {
			logger.info(e);
		}
		return status;
	}
}
